require File.expand_path("#{File.dirname(__FILE__)}/../config")

module DefaultBenderTest
  # Nullify Bender configuration
  def reset_config
    Bender.instance_variable_set :@config, nil
    Bender.instance_variable_set :@bot, nil
  end

  def protect_config_file
    FileUtils.cp_r @test_config_file, @test_config_file + '.copy', :remove_destination => true
    Bender.config @test_config_file
  end

  def config_file_is_save
    FileUtils.cp_r @test_config_file + '.copy', @test_config_file, :remove_destination => true
    FileUtils.rm @test_config_file + '.copy'
  end

  # Will skip test, using appropriate method call
  def ignore(message)
    if RUBY_VERSION >= '1.9.0'
      skip(message)
    else
      assert false, "Skipping test: #{message}"
    end
  end

  # Skip test unless cloning test are specified to be called
  def ignore_unless_cloning(message)
    ignore(message) unless BenderTestConfig.clone_repo?
  end

  def ignore_unless_massaging(message)
    ignore(message) unless BenderTestConfig.messaging?
  end

end
