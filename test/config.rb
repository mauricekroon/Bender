require 'test/unit'
require 'bundler'
require 'debugger' if RUBY_VERSION >= '1.9.0'
require 'ruby-debug' if RUBY_VERSION < '1.9.0'

#ENV['clone_test'] = 'test'

Bundler.setup(:default, :test)

$:.push File.expand_path("../lib", File.dirname(__FILE__))
require 'bender'

#Cruse control rb can not be used as a gem.... =( hence to test it the models were copied here
Dir[File.join(File.dirname(__FILE__),'support','models','**/*.rb')].each {|f| require f }

class BenderTestConfig
  class << self
    attr_accessor :clone_repo
    alias_method :clone_repo?, :clone_repo

    attr_accessor :messaging
    alias_method :messaging?, :messaging
  end
end

BenderTestConfig.clone_repo = false
BenderTestConfig.messaging = true