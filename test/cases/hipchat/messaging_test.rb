require File.expand_path("#{File.dirname(__FILE__)}/../../support/default_bender_test")

class BotMesagingTest < Test::Unit::TestCase
  include DefaultBenderTest
  
  def setup
    reset_config
    Bender.config File.expand_path("config/test.yml")
    c = Bender.config['bender']
    @bot = Bender::Hipchat.new c['jid'], c['nick'], c['room'], c['pass']
  end

  def test_connection
    ignore_unless_massaging("Connection Test")
    @bot.send :connect
    assert @bot.active?
  end

  def test_bot_messaging
    ignore_unless_massaging("Messaging test")
    Thread.new do
      @bot.send :connect
      sleep 1
      @bot.say 'Test'
      sleep 1
    end
    secconds = 10
    interval = 0.5
    (secconds/interval).to_i.times {sleep interval}
  end
end