require File.expand_path("#{File.dirname(__FILE__)}/../../support/default_bender_test")

class BotSimpleTest < Test::Unit::TestCase
  include DefaultBenderTest

  def setup
    reset_config
    Bender.config File.expand_path("config/test.yml")
  end

  def test_bot_creates_with_no_exceptions
    assert_nothing_thrown do
      Bender.bot
    end
  end

  def test_bot_is_wrapped_with_bender
    assert Bender.bot.kind_of? Bender::Hipchat
  end

  def test_bot_respond_to_m
    assert_respond_to Bender.bot, :say
  end
end
