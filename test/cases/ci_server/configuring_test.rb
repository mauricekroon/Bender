require File.expand_path("#{File.dirname(__FILE__)}/../../support/default_bender_test")

class CiConfiguringTest < Test::Unit::TestCase
  include DefaultBenderTest

  def setup
    reset_config
   
    @test_config_file = File.expand_path("config/test.yml")
    protect_config_file

    @new_project = {:name => 'Studentify', :config => { :origin => 'git@github.com:Studentify/Studentify.git',
        :branch => 'master',
        :production => '',
        :staging => ''}}

    @config = Bender::Git::Config.parse Bender.config
  end

  def teardown
    config_file_is_save

    remove_project
  end

  def remove_project
    config = Bender::CiServer.projects[@new_project[:name]]
    FileUtils.rm_rf(File.expand_path(config['path'][0..-5]))
  end

#  def test_loading_config
#    assert Bender::CiServer.config.nil?
#    assert_nothing_thrown do
#      Bender::CiServer.load_config @config
#    end
#    assert_not_nil Bender::CiServer.config
#    assert Bender::CiServer.config.kind_of?(Hash)
#  end
#
#  def test_loading_worng_object_for_config
#    assert Bender::CiServer.config.nil?
#    assert_raise Bender::CiServer::InvalidConfugirationObject do
#      Bender::CiServer.load_config Object.new
#    end
#    assert_nil Bender::CiServer.config
#  end
#
#  def test_reloading_existed_config
#    assert Bender::CiServer.config.nil?
#
#    Bender::CiServer.load_config @config
#
#    assert_raise Bender::CiServer::ConfigurationLoaded do
#      Bender::CiServer.load_config @config
#    end
#  end
#
#  def test_reset_config
#    assert Bender::CiServer.config.nil?
#
#    Bender::CiServer.load_config @config
#    Bender::CiServer.reset_config!
#
#    assert Bender::CiServer.config.nil?
#    assert_nothing_thrown do
#      Bender::CiServer.load_config @config
#    end
#    assert_not_nil Bender::CiServer.config
#    assert Bender::CiServer.config.kind_of?(Hash)
#  end
#
#  def test_define_new_project
#    Bender::CiServer.load_config @config
#    assert_not_nil Bender::CiServer.config
#
#    assert_equal false, Bender::CiServer.send(:project_defined?, @new_project[:name])
#    ci_instance = Bender::CiServer.define_project @new_project[:name], @new_project[:config]
#    assert_equal true, Bender::CiServer.send(:project_defined?, @new_project[:name])
#
#    assert @config.registered?('Studentify'), 'Registered project is missing'
#    assert ci_instance.kind_of?(Bender::CiServer), "Expected Bender::CiServer, but was #{ci_instance.class}"
#
#    remove_project
#  end
#
  def test_force_build
    Bender::CiServer.load_config @config
    ci_instance = Bender::CiServer.define_project @new_project[:name], @new_project[:config]
    assert !ci_instance.active?
    assert ci_instance.force_build
    sleep 5
    assert ci_instance.active?
  end

end