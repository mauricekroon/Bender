# -*- coding: utf-8 -*-
require File.expand_path("#{File.dirname(__FILE__)}/../support/default_bender_test")

class APriorityConfigurationTest < Test::Unit::TestCase
  include DefaultBenderTest

  def setup
    reset_config
  end

  def test_have_default_config_file
    config = Bender.config

    assert_not_nil config['bender']
    assert_not_nil config['bender']['nick']
  end

  def test_accept_another_config_file
    config = Bender.config File.expand_path("config/test.yml")

    assert_not_nil config['bender']
    assert_not_nil config['bender']['nick']
  end

  def test_raise_error_on_file_abscense
    assert_raise Exception do
      Bender.config File.expand_path("../config/test.yml",File.dirname(__FILE__))
    end
  end

  def test_has_project_config
    Bender.config File.expand_path("config/test.yml")
    assert_nothing_raised do
      Bender.projects_configs
    end
    assert !Bender.projects_configs.include?('bender')
    assert Bender.projects_configs.include?('StudentTutorsNew')
  end

  def test_creates_with_default_config
    assert_nothing_raised do
      Bender.bot
    end
  end

end