require File.expand_path("#{File.dirname(__FILE__)}/../../support/default_bender_test")

class ChatRepoTest < Test::Unit::TestCase
  include DefaultBenderTest

  def setup
    reset_config
    Bender.config File.expand_path("config/test.yml")
    @client = Bender::Git.new 'StudentTutorsNew'
    c = @client.instance_variable_get :@config
    @path = @client.working_dir_absolute
  end

  def test_read_access_to_repo
    assert_nothing_thrown do
      assert @client.default_branch_exists?
    end
  end

  def test_read_access_to_repo
    assert !@client.remote_branch_exists?('unexisting_branch')
  end

  def test_absolute_path
    assert @client.working_dir_absolute != @client.working_dir_relative
    assert @client.working_dir == File.expand_path( @client.working_dir_relative )
  end

  def test_has_working_dir
    assert_not_nil @client.working_dir
  end

  def test_branch_existance
    assert @client.default_branch_exists?
    assert @client.remote_branch_exists?('staging')
    assert @client.remote_branch_exists?('development')
    assert !@client.remote_branch_exists?('non_existing_branch')
  end
end