require 'fileutils'
require File.expand_path("#{File.dirname(__FILE__)}/../../support/default_bender_test")

class GitMultipleProjectTest < Test::Unit::TestCase
  include DefaultBenderTest

  def setup
    reset_config
    @test_config_file = File.expand_path("config/test.yml")
    FileUtils.cp_r @test_config_file, @test_config_file + '.copy', :remove_destination => true
    Bender.config @test_config_file
  end

  def teardown
    FileUtils.cp_r @test_config_file + '.copy', @test_config_file, :remove_destination => true
    FileUtils.rm @test_config_file + '.copy'
  end

  def test_register_new_project
    Bender::Git.define_new_project('StudentTutorsNew1', { :origin => 'git@github.com:Studentify/StudentTutorsNew.git',
      :branch => 'master',
      :path =>  'tmp/StudentTutorsNew',
      :production =>    '',
      :staging =>      ''})
    
    reset_config
    Bender::Git.instance_variable_set :@config, nil
    
    Bender.config @test_config_file
    generated_config = YAML.load File.read(@test_config_file)

    assert Bender::Git.registered?('StudentTutorsNew1'), 'Registered project is missing'
    assert generated_config.has_key?('StudentTutorsNew1'), 'Registered project is mnot included into list'
  end
end
