require File.expand_path("#{File.dirname(__FILE__)}/../../support/default_bender_test")

class APriorityGitRepoCloneTest < Test::Unit::TestCase
  include DefaultBenderTest
  
  def setup
    reset_config
    @test_config_file = File.expand_path("config/test.yml")
    protect_config_file
  end

  def teardown
    config_file_is_save
  end

  def test_agent_is_git
    ignore_unless_cloning('Skip repo cloning test')
    repo_name = 'StudentTutorsNew'
    client = Bender::Git.new 'StudentTutorsNew'
    client.send(:reset_repo) if Bender::Git.registered? repo_name
    assert_nothing_thrown do
      client =  Bender::Git.new 'StudentTutorsNew'
    end
    assert File.exists?(client.working_dir_absolute)
    assert File.exists?(File.join(client.working_dir_absolute,'.git'))
    assert_equal ['origin','production','staging'], client.send(:remote_names, true)
    assert !client.send(:agent).is_local_branch?('staging')
    assert !client.send(:agent).is_local_branch?('development')
    client.send :on_branch, 'staging' do |a|; end
    client.send :on_branch, 'development' do |a|; end
    assert client.send(:agent).is_local_branch?('staging')
    assert client.send(:agent).is_local_branch?('development')
  end

  def test_project_registration
    ignore_unless_cloning('Skip repo cloning test')
    repo_name, config, client = 'Studentify', { :origin => 'git@github.com:Studentify/Studentify.git',
      :branch => 'master', :path =>  'tmp/Studentify', :production => '', :staging => ''}, nil
    FileUtils.rm_rf(File.expand_path(config[:path])) if File.exist?('tmp/Studentify')
    assert_nothing_thrown do
      client = Bender::Git.define_new_project repo_name, config
      client
    end
    assert File.exists?(client.working_dir_absolute)
    assert File.exists?(File.join(client.working_dir_absolute,'.git'))
    assert_equal ['origin','production','staging'], client.send(:remote_names, true)
    assert !client.send(:agent).is_local_branch?('staging')
    assert !client.send(:agent).is_local_branch?('development')
    client.send :on_branch, 'staging' do |a|; end
    client.send :on_branch, 'development' do |a|; end
    assert client.send(:agent).is_local_branch?('staging')
    assert client.send(:agent).is_local_branch?('development')

    reset_config
    Bender::Git.instance_variable_set :@config, nil

    Bender.config @test_config_file
    generated_config = YAML.load File.read(@test_config_file)

    assert Bender::Git.registered?(repo_name), 'Registered project is missing'
    assert generated_config.has_key?(repo_name), 'Registered project is mnot included into list'
  end
end