$:.push File.expand_path(File.dirname(__FILE__)) unless $:.include?(File.expand_path(File.dirname(__FILE__)))

require 'rubygems'
require 'happy_fun_time_bot'
require 'git/ls-remote'
require 'yaml'
require 'active_support/core_ext'

module Bender
  autoload :Google, 'bender/google'
  autoload :WorkingDir, 'bender/working_dir'

  autoload :Hipchat, 'bender/hipchat'
  autoload :Heroku, 'bender/heroku'
  autoload :Git, 'bender/git'
  autoload :Formatter, 'bender/formatter'
  autoload :CiServer, 'bender/ci_server'

  class << self
    attr_reader :config_path
    
    # Parse and keep Bender configurations.
    # Accepts abosulte file path to configuration file.
    # Default config file: root/config/bender.yml
    def config config_path = nil
      @config ||= begin
        @config_path = config_path || File.expand_path("../config/bender.yml",File.dirname(__FILE__))
        raise Exception.new("Config file is missing") unless File.exists? @config_path
        config = YAML.load File.read(@config_path)
        Bender::Git.parse_cofig config
        config
      end
    end

    # Keeps bot instance, if it is missing it creates one.
    def bot
      @bot ||= begin
        c = config['bender']
        Bender::Hipchat.new c['jid'], c['nick'], c['room'], c['pass']
      end
    end
    
  end
end
