module Bender
  module WorkingDir
    attr_reader :config
    
    def working_dir_absolute
      File.expand_path( working_dir_relative )
    end
    alias_method :working_dir, :working_dir_absolute

    def working_dir_relative
      config['path'] || config[:path]
    end
  end
end