module Bender
  ###
  # Heroku class deals purely with Heroku service.
  # In include only deploying and nothing more.
  # A brief example of how to deal with this class.
  #
  #   heroku_instance = Heroku.new 'MyFancyProject'
  #   or
  #   heroku_instance = Heroku.new 'path/to/repository', {production: 'http://production', staging: 'http://staging'}
  #
  # To deploy the application simply call:
  #
  #   heroku_instance.deploy_to_production [branch]
  #     - in order to deploy to production. If branch is pecified it will deploy a specific branch
  #   heroku_instance.deploy_to_staging [branch]
  #     - in order to deploy to staging. If branch is pecified it will deploy a specific branch
  #
  class Heroku

    ###
    # Creates new project instance to deploy to Heroku.
    # Examples:
    #
    #   Heroku.new 'MyFancyProject' # => <Bender::Heroku ...>
    #     - will obtain all details from configuration file
    #   Heroku.new 'path/to/repository' # => <Bender::Heroku>
    #     - will create a new instance that might be tried deployed
    #
    # Raises
    #
    #   ProjectNotExists - in case project is not listed in the config file
    #   ProjectConfigCorrupted - In case if something wrong with configuration file
    #   RepoCorrupted - if repo is miising or there is no valid project inside
    #
    def initialize project_name_or_path, opts = {}
      
    end
  end
end
