module Bender
  ###
  # Hipchat class is dealing with messaging.
  # It has to big parts:
  #
  #   Listeners -  which are handling specific massages sent by others
  #   and
  #   Responders - those are contating specific information for chat
  #
  # TO create new connection:
  #
  #   hipcha_bot = Hipchat.new jid, nick, room, passowd
  #
  # For public Api it has only one method:
  #
  #  hipchat_bot.say service_name, message_content
  #    - that will output massage in the following format "service_name: message_content"
  #
  class Hipchat

    def initialize jid, nick, room, passwd
      @nick = nick
      @bot = begin
        bot = ::HappyFunTimeBot.new(:jid => jid,
          :nick => nick,
          :room => room,
          :password => passwd)
        if $VERBOSE == 'chat'
          def bot.say(msg); send_response(msg); end
        else
          def bot.say(msg); puts msg; end
        end
        def bot.bot_say(msg); send_response("Bot: #{msg}"); end
        def bot.disconnect; self.client.close; end
        bot
      end
    end

    ###
    # Chack whether the client is still active
    def active?
      @bot.muc.active?
    end

    def say system_name, msg
      @bot.say("#{system_name}: #{msg}");
    end

    def run!
      @thread ||= Thread.new do
        @bot.connect
      end
    end

    protected

    ###
    # set the connection to bot
    def connect
      load_responders
      @bot.connect
      sleep 0.5
      @bot.bot_say("#{@nick} connected and ready for operating")
      sleep 0.5
    end

    ###
    # define responders
    def load_responders
      require 'bender/hipchat/listeners/test'
      Bender::Hipchat::Listeners::Test.register_responders @bot
    end
  end
end