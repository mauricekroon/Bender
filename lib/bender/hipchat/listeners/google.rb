class Bender
  module Responders
    module Google
      # let's find some images!
      bot.add_responder('findimage') do |from, args|
        url = Google.get_image_url(args)
        "Look! #{url}"
      end
    end
  end
end

::Bender.send :include, Bender::Responders::Google
