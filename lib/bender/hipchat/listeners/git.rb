class Bender
  module Responders
    module Git
      bot.add_responder('safety_dance') do |from, args|
        client = Bender::Git::Clent.new
        begin
          raise("Branch #{branch} does not exist!") if client.remote_branch_exists?(branch = args.first)
          client.merge_into_staging branch
          #if merge_fail client.say "Merge produced conflict"
          client.on_staging do
            #cijoe thing here
            #if cijoe_success 
            #  client.say "Tests passed"
            #  msg = "Releasing..."
            #else
            #  raise "Tests not passed. For more information check out #{link}"
            #  
            #end
          end
        rescue
          client.say $!.message
          msg = "Attempt failed"
        end
      end

      bot.add_responder('danger_zone') do |from, args|
        msg = ''
        begin
          Bender::Git.agent.fetch
          Bender::Git.agent.checkout(Bender.config['git']['branch'])
          Bender::Git.agent.merge('origin/master')
          #    @git.push(@git.remote('production'))
          system "echo 'Deploy => #{Time.now.utc}\n' >> #{@logfile}"
          system "cd #{working_dir} && git checkout master && git merge origin/master && git push production master >> #{@logfile}"
        rescue
          msg = "@#{from} something went wrong. IT'S ALL YOUR FAULT."
        else
          msg = "#{from} deployed #{args} to production"
        ensure
          Bender::Git.agent.checkout('master')
        end
        "#{msg}\n#{%x[cat #{@logfile} | tail -#{@last_lines}]}"
      end
    end
  end
end

::Bender.send :include, Bender::Responders::Git