
class Bender::Hipchat
  module Listeners
    class Test
      class << self
        def register_responders bot
          register_test bot
          register_fun bot
        end

        protected

        def register_test bot
          bot.add_responder('heybot') do |from, args|
            "Oh HAI #{from}!!!"
          end
        end

        def register_fun bot
          bot.add_responder('bot') do |from, args|
            case args.join(' ')
            when 'you are bad'
              'Yes, I am!'
            when 'protect me'
              'Hay, you People! BACK TO WORK!!!!'
            when /^say_to/
              if from =~ /Max/
                "I can not affend Mein Führer, so Fuck you #{from}!"
              else
                "Yu are a bad person, #{args.second}"
              end
            when /^test/
              'Test me master'
            when 'say_them!'
              'Work, B.\'s!'
            when 'are_you_with_me?'
              "Jawohl, Mein Fuhrer!"
            end
          end
        end
      end


    end
  end
end
