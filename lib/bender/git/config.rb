module Bender
  class Git::Config
    class << self
      def parse dirty_cofig
        dirt = {}
        clean_config = {}
        
        dirty_cofig.each_pair do |name, configurs|
          if minimal_info_included? configurs
            clean_config.merge!(name => configurs)
          else
            dirt.merge!(name => configurs)
          end
        end

        new clean_config, :dirt => dirt
      end
      
      protected
      
      def minimal_info_included? elem
        required_keys = %w(origin branch path production).freeze
        keys = elem.keys.collect(&:to_s)
        required_keys.each do |key|
          return false unless keys.include? key
        end
        true
      end
    end
    attr_reader :projects, :dirt

    def initialize projects, opts = {}
      @projects = projects
      @registered_projects = @projects.keys
      @dirt = opts.delete(:dirt)
    end

    def dump file_name = nil
      if file_name
        File.open( file_name, 'w' ) do |out|
          out.write YAML.dump( @dirt.merge(@projects) )
        end
        nil
      else
        @dirt.merge(@projects).to_yaml
      end
    end
    
    def register project_name, config
      config = Bender::Formatter.stringify_hash(config)
      if valid_project? config
        if @registered_projects.include?(project_name)
          return
        else
          @projects.merge!(project_name => config)
          @registered_projects << project_name
        end
      else
        raise "Missing key infornmation: #{config.inspect}"
      end
    end

    def registered? project_name
      @registered_projects.include? project_name
    end
    
    protected
    
    def valid_project? config
      self.class.send(:minimal_info_included?, config)
    end

  end
end