require 'git'
require 'fileutils'

module Bender
  ## Aimed to deal with git
  # This part is almost done
  # Basic application
  #
  #   # to create git client instance
  #   bender = ::Bender::Git.new(config)
  #
  #   # to say something into chat
  #   bender.say "I am initiated"
  #
  #   # to do some stuff on master branch
  #   bender.on_master do
  #     # fancy actions
  #   end
  #
  #   # to pull remote changes into local
  #   bender.update 'branch' # if that branch did not exists locally, it will be pulled
  #
  #   # to merge master into some branch
  #   bender.merge_master_into 'new-feature'
  #
  #   # to mergse some branch into master
  #   bender.merge_into_master 'new-feature'
  #
  class Git
    include WorkingDir

    autoload :Config, 'bender/git/config'

    class << self
      attr_reader :config

      ###
      # Parse provided config and create configuration Hash
      def parse_cofig unprepared_array
        @config ||= Bender::Git::Config.parse unprepared_array
      end

      ###
      # Add new project into cofiguration and dump new version of config
      def define_new_project project, config
        @config.register project, config
        @config.dump Bender.config_path
        
        new project
      end

      ###
      # Check whether the the project is registered or not
      def registered?(*opts); config.registered?(*opts); end
    end

    attr_reader :name

    def initialize project_name
      raise('Project not registered') unless self.class.config.registered?(project_name)
      @name = project_name
      @config = Bender::Formatter.stringify_hash(self.class.config.projects[project_name])

      clone_origin unless repo_exists?
      define_remotes unless production_specified?
    end

    def say message
      Bender.bot.say "Git agent", message
    end

    ###
    # Checks default branch presence on remote
    def default_branch_exists?
      remote_branch_exists?
    end

    ###
    # Checks branch presence on remote
    # If no branch name is provided will use default branch from config
    def remote_branch_exists? branch_name = nil
      branch_name ||= config['branch']
      @lib ||= ::Git::Lib.new
      @repo_details ||= @lib.ls_remote(config['origin'])
      @repo_details['branches'].include?(branch_name.to_s)
    end

    ###
    # Perform actions on master branch
    def on_master &block
      on_branch 'master', &block
    end

    ###
    # Perform actions on staging branch
    def on_staging &block
      on_branch 'staging', &block
    end

    ###
    # Update remote branch by pulling a code form it, into local branch
    def update branch
      on_branch branch do |a|
        say "Pulling from #{branch}..."
        a.pull(a.remote('origin'), branch)
      end
    end

    ###
    # Pull latest changes from branch on remote into master branch
    def merge_into_master branch
      merge branch, 'master'
    end
      
    ###
    # Pull latest changes from branch on remote into master branch
    def merge_master_into branch
      merge 'master', branch
    end

    ###
    # Pull latest changes from branch on remote into master branch
    def merge_into_staging branch
      merge branch, 'staging'
    end

    def conflicting?
      `git status` =~ /CONFLICT/i
    end
    
    protected

    # Git instance
    def agent
      @agent ||= ::Git.open(working_dir_relative)
    end

    ###
    # Merging one branch into another
    def merge branch, into_branch
      on_branch branch do |a|
        say "Merging #{branch} into #{into_branch}"
        a.merge(into_branch)
        if conflicting?
          say("Merging has result into conflicts. Rolling back")
          # Reseting actions `git reset --hard HEAD`
        else
          say("Merged successfully")
        end
      end
    end

    ###
    # Perform some actions under specific branch
    # Block will receive Agent instance
    def on_branch branch, &block
      agent.fetch
      unless agent.is_local_branch?(branch)
        if remote_branch_exists?(branch)
          agent.branch(branch)
        else
          say "Branch '#{branch}' does not exists neither al local nor at remote."
          return
        end
      end
      agent.checkout(branch)
      yield(agent)
      agent.checkout('master')
    end
      
    ###
    # This will clone repository for remote source.
    # NB! If repository exists, it will be removed.
    def clone_repository
      reset_repo if repo_exists?
      say "Cloning repo #{config['origin']} into #{working_dir_absolute}"
      ::Git.clone(config['origin'], working_dir_absolute)
      if repo_exists?
        say "Cloned successfully"
      else
        say "Cloning falied"
      end
    end

    ###
    # Should clone Origing into newly created folder
    def clone_origin
      ::Git.clone(config['origin'], name, :path => File.dirname(working_dir_absolute))
    end

    ###
    # Adds remote repositories into git config
    def define_remotes
      agent.add_remote('production', config['production']) unless remote_specified?('production')
      agent.add_remote('staging', config['staging']) unless remote_specified?('staging')
    end

    ###
    # Returns whether remote origin with specific name is specified
    def remote_specified? name
      remote_names.include? name
    end

    ###
    # Returns whether origin is set
    def production_specified?
      remote_names.include? 'production'
    end

    ###
    # Returns array of specified names of remotes
    def remote_names refresh = false
      @remote_names = nil if refresh
      @remote_names ||= agent.remotes.collect {|r| r.name}
    end

    ###
    # Removes directory in case it exists
    def reset_repo
      FileUtils.rm_rf(working_dir_absolute)
    end

    ###
    # Checks whether repository exists or not
    def repo_exists?
      File.exists?(working_dir_absolute)
    end
  end
end