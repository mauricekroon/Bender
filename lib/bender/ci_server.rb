require 'net/http'
require 'uri'

module Bender
  ###
  # CiServer class is dedicated to dealing with CruiseControl.rb configurations
  # In order to do so, the first thing is to know where the configuration file
  # is stored.
  # To set the Configuration file path:
  #
  #   CiServer.config_path = 'path/to/cofig/file'
  #
  # After the configuration file is provided, a new project might be initialized:
  #
  #   ci_settings = CiServer.new 'path/to/repository'
  #
  # NB! If that project is missing from CC.rb configuration file, it will be
  # added with default parameters.
  #
  # In order to change any value:
  #
  #   ci_settings.set :key, value
  #
  # For options see:
  #
  #   http://cruisecontrolrb.thoughtworks.com/documentation/manual#project_builder_configuration
  #
  class CiServer
    class InvalidConfugirationObject < Exception; end
    class ConfigurationLoaded < Exception; end

    class << self
      def config; @config; end
      def projects; @projects; end

      ###
      # Reset configuration file, so it later may be overwritten
      def reset_config!
        @config = nil
      end

      ###
      # Congif suppose to be an instance of Bender::Git::Config
      def load_config config
        raise CiServer::InvalidConfugirationObject.new("Expected configuration kind of Bender::Git::Config, got #{config.class.name}") unless config.kind_of?(Bender::Git::Config)
        raise CiServer::ConfigurationLoaded.new("Config is loaded. Please reset it, before reloading") unless @config.nil?
        @config = config.dirt['cruise_control']
        @projects = config.projects
        @config_object = config
      end

      ###
      # Define project
      def define_project project_name, project_config
        project_config = Bender::Formatter.stringify_hash project_config
        if cc_running?
          define_project_online project_name, project_config
        else
          define_project_offline project_name, project_config
        end

        if project_defined? project_name
          @config_object.register project_name, project_config.merge({:path => "#{config['projects']}/#{project_name}/work"})
          @config_object.dump Bender.config_path

          new project_name
        end
      end

      protected

      def http_object
        @http_object ||= begin
          uri = URI.parse(config['url'])
          Net::HTTP.new(uri.host, uri.port)
        end
      end

      ###
      # Check weither CruiseControlRb is up and running
      def cc_running?
        begin
          resoponce = http_object.get('/')
          resoponce.is_a?(Net::HTTPSuccess)
        rescue Errno::ECONNREFUSED
          false
        end
      end

      def cc_path
        config['root']
      end

      ###
      # Check whether project is defined in cruise control
      def project_defined? project_name
        dir_path = File.expand_path(config['projects'])
        entires = Dir.entries(dir_path)
        entires.select {|e| return true if e =~ /\A#{project_name}\Z/}
        false
      end

      ###
      # Define project, providing this info via bash
      def define_project_offline project_name, config
        nil #there are some problems, as cruise control constantly returns error
        unless project_defined? project_name
          command = "(./add_cruise_project #{cc_path} #{project_name} #{config['origin']} &) &"
          system command

          new project_name
        end
      end

      ###
      # Define project, providing key info via http request
      def define_project_online project_name, config
        unless project_defined? project_name
          data = "project%5Bname%5D=#{project_name}&project%5Bsource_control%5D%5Brepository%5D=#{CGI.escape(config['origin'])}&project%5Bsource_control%5D%5Bsource_control%5D=Git"
          #params = {:project => { :name => project_name, :source_control => {:repository => config['origin'], :source_control => 'Git'}}}
          responce = http_object.post("/projects", data)#params.to_query)
          ['200','302'].include? responce.code
        end
      end
    end

    attr_reader :name

    def initialize project_name
      @name = project_name
    end

    ###
    # Will send a build request to ci_server
    def force_build
      http_object = self.class.send :http_object
      responce = http_object.post("/projects/#{name}/build",'')#params.to_query)
      #sleep 2
      #http_object.finish
      ['200','302'].include? responce.code
    end

    ###
    # Returns, whether the project is currently building or not.
    def building?
      builder_status == 'building'
    end

    def active?
      status = builder_status
      !status.nil? && status != 'sleeping'
    end

    ###
    # Returns, whether last build was successful
    def last_build_successful?
      last_build_status == 'success'
    end

    protected

    def builder_status
      if(builder_file = list_whole_folder.select { |e| e =~ /builder_status\.[a-z]{0,32}/}.first)
        builder_file.match(/builder_status\.([a-z]{0,32})/)[1]
      end
    end

    def list_whole_folder
      `ls -Alt --full-time #{self.class.config['projects']}/#{name}`.split("\n")
    end

    def last_build_status
      if(build = list_whole_folder.select { |e| e =~ /build-#{last_build}-[a-z]{5,15}\.in(\d+)s\Z/}.first)
        build.match(/#{last_build}-([a-z]{5,25})\.in(\d+)s\Z/)[1]
      end
    end

    def last_build
      if(build = list_whole_folder.select { |e| e =~ /build-[a-z0-9]{40}\.[0-9]{1,2}\Z/}.first)
        build.match(/([a-z0-9]{40})/)[1]
      end
    end
  end
end
