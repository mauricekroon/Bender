require 'git'

module Git
  #Unless pull request #37 is merged, need to use this hook
  module LsRemote
    def ls_remote(location=nil)
      Hash.new{ |h,k| h[k] = {} }.tap do |hsh|
        command_lines('ls-remote', [location], false).each do |line|
          (sha, info) = line.split("\t")
          (ref, type, name) = info.split('/', 3)
          type ||= 'head'
          type = 'branches' if type == 'heads'
          value = {:ref => ref, :sha => sha}
          hsh[type].update( name.nil? ? value : { name => value })
        end
      end
    end
  end
end

::Git::Lib.send :include, ::Git::LsRemote
